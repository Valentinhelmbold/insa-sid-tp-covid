# insa-SID-TP-covid

## Objectifs
Un projet pour voir :
* une architecture client serveur (un peu distribuée)
* une API très simple

### Quêtes annexes
* travailler à plusieurs avec git
* quelques notions de choix stratégiques de stop covid…

## Bibliographie
* https://risques-tracage.fr/docs/risques-tracage.pdf
* https://github.com/DP-3T/documents/blob/master/public_engagement/cartoon/fr/combined_panels.pdf

## Librairies utilisées
* requests : https://riptutorial.com/fr/python-requests
* flask : https://flask.palletsprojects.com/en/1.1.x/installation/
* venv : https://virtualenv.pypa.io/en/latest/installation.html   
([installer flask dans l'environnement](https://flask.palletsprojects.com/en/1.1.x/installation/#virtual-environments))

## Crédits image
https://commons.wikimedia.org/wiki/File:SARS-CoV-2_without_background.png

## Mise en place de l'environnement
voir sujet ou la [documentation flask](https://flask.palletsprojects.com/en/1.1.x/installation/) pour les différences de système d'exploitation…
1. Créer un fork pour votre groupe…
2. Cloner votre projet
3. Installer virtualenv (si pas encore fait)
4. Créer un environnement avec la commande  
``python3 -m venv venv-TPSID`` (avec “venv-TPSID” le nom de votre environnement)
5. Activer l'environnement  
``source venv-TPSID/bin/activate`` (Linux/Mac)  
``venv-TPSID\Scripts\activate`` (Windows)
6. [Installer flask](https://flask.palletsprojects.com/en/1.1.x/installation/#install-flask)
7. Commencer à travailler

**NB** : Si l'un des membres du projet effectue les actions 4 à 6 et envoie ses modifications sur le dépôt (``git push origin master``) et que les autres membres du projet téléchargent ces modifications (``git pull origin master``), ils n'ont (__normalement__) besoin **que** d'effectuer l'action 5.
